<?php

/**
 * Implements hook_theme().
 */
function field_group_table_wrapper_theme() {
  return array(
    'field_group_table_wrapper_render' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements hook_field_group_formatter_info().
 */
function field_group_table_wrapper_field_group_formatter_info() {
  return array(
    'display' => array(
      'table-wrapper' => array(
        'label' => t('Table wrapper'),
        'description' => t('This fieldgroup renders child table rows in its own table HTML wrapper.'),
        'instance_settings' => array(
          'classes' => '',
          'id' => '',
          'caption' => '',
          'tfoot' => '',
          'label_as_header' => 0,
          'column_headers' => '',
          'column_separator' => '|',
        ),
      ),
      'table-row' => array(
        'label' => t('Table row'),
        'description' => t('This fieldgroup renders table rows within a Table wrapper.'),
        'instance_settings' => array(
          'classes' => '',
          'label_first_column' => 0,
          'separate_label_vs_field' => 0,
          'first_column' => '',
          'hide_title' => FALSE,
          'colspan' => '',
          'css_style' => '',
        ),
      ),
    ),
    'form' => array(
      'table-wrapper' => array(
        'label' => t('Table wrapper'),
        'description' => t('This fieldgroup renders child table rows in its own table HTML wrapper.'),
        'instance_settings' => array(
          'classes' => '',
          'id' => '',
          'caption' => '',
          'tfoot' => '',
          'label_as_header' => 0,
          'column_headers' => '',
          'column_separator' => '|'
        ),
      ),
      'table-row' => array(
        'label' => t('Table row'),
        'description' => t('This fieldgroup renders table rows within a Table wrapper.'),
        'instance_settings' => array(
          'classes' => '',
          'label_first_column' => 0,
          'separate_label_vs_field' => 0,
          'first_column' => '',
          'hide_title' => TRUE,
          'colspan' => '',
          'css_style' => ''
        ),
      ),
    ),
  );
}

/**
 * Implements hook_field_group_format_settings().
 */
function field_group_table_wrapper_field_group_format_settings($group) {
  // Add a wrapper for extra settings to use by others.
  $form = array(
    'instance_settings' => array(
      '#tree' => TRUE,
      '#weight' => 2,
    ),
  );

  $field_group_types = field_group_formatter_info();
  $mode = $group->mode == 'form' ? 'form' : 'display';
  $formatter = $field_group_types[$mode][$group->format_type];
  $settings = $group->format_settings['instance_settings'];

  // Add optional instance_settings.
  switch ($group->format_type) {
    case 'table-wrapper':
      $form['instance_settings']['label_as_header'] = array(
        '#title' => t('Use the Field group label as header'),
        '#type' => 'checkbox',
        '#return_value' => 1,
        '#default_value' => isset($settings['label_as_header']) ? $settings['label_as_header'] : $formatter['instance_settings']['label_as_header'],
        '#description' => t('If checked the Column headers will be ommitted and field group label will be used as header of table.'),
      );
      $form['instance_settings']['caption'] = array(
        '#title' => t('Caption of table'),
        '#type' => 'textfield',
        '#default_value' => isset($settings['caption']) ? $settings['caption'] : $formatter['instance_settings']['caption'],
      );
      $form['instance_settings']['tfoot'] = array(
        '#title' => t('Footer of table'),
        '#type' => 'textarea',
        '#default_value' => isset($settings['tfoot']) ? $settings['tfoot'] : $formatter['instance_settings']['tfoot'],
      );
      $form['instance_settings']['column_headers'] = array(
        '#title' => t('Column headers'),
        '#type' => 'textarea',
        '#default_value' => isset($settings['column_headers']) ? $settings['column_headers'] : $formatter['instance_settings']['column_headers'],
        '#description' => t('Provides the names of the columns separating them by character defined in Column separator field. If Table Row identifier is given (group_xyz), the label of Table Row will be used. Colspan for header can be defined by following formula: "header_1{colspan:2}|header_2{colspan:3}"'),
      );
      $form['instance_settings']['column_separator'] = array(
        '#title' => t('Column spearator'),
        '#type' => 'select',
        '#options' => array('|' => '|', ';' => ';', '#' => '#', '--' => '--'),
        '#default_value' => isset($settings['column_separator']) ? $settings['column_separator'] : $formatter['instance_settings']['column_separator'],
        '#weight' => 2,
      );
      break;

    case 'table-row':
      $form['instance_settings']['label_first_column'] = array(
        '#title' => t('Use group label as first column'),
        '#type' => 'checkbox',
        '#return_value' => 1,
        '#default_value' => isset($settings['label_first_column']) ? $settings['label_first_column'] : $formatter['instance_settings']['label_first_column'],
        '#weight' => 2,
      );
      $form['instance_settings']['separate_label_vs_field'] = array(
        '#title' => t('Separated label with field - Make two columns for each fields: one for label, one for field'),
        '#type' => 'checkbox',
        '#return_value' => 1,
        '#default_value' => isset($settings['separate_label_vs_field']) ? $settings['separate_label_vs_field'] : $formatter['instance_settings']['label_first_column'],
        '#weight' => 3,
      );
      $form['instance_settings']['first_column'] = array(
        '#title' => t('First column'),
        '#type' => 'textfield',
        '#default_value' => isset($settings['first_column']) ? $settings['first_column'] : $formatter['instance_settings']['first_column'],
        '#description' => t('You can define a custom first column for the row if necessary.'),
        '#weight' => 4,
      );
      $form['instance_settings']['hide_title'] = array(
        '#title' => t('Hide title of cells'),
        '#type' => 'select',
        '#options' => array(FALSE => t('No'), TRUE => t('Yes')),
        '#default_value' => isset($settings['hide_title']) ? $settings['hide_title'] : $formatter['instance_settings']['hide_title'],
        '#weight' => 5,
      );
      $form['instance_settings']['colspan'] = array(
        '#title' => t('Colspan'),
        '#type' => 'textfield',
        '#default_value' => isset($settings['colspan']) ? $settings['colspan'] : $formatter['instance_settings']['colspan'],
        '#description' => t('Provides colspan information for elements of row. Format: field_name_1:2|field_name_2:3 It means field_name_1 has 2 colspans, field_name_2 has 3 colspans'),
        '#weight' => 6,
      );
      $form['instance_settings']['css_style'] = array(
        '#title' => t('CSS Style'),
        '#type' => 'textarea',
        '#default_value' => isset($settings['css_style']) ? $settings['css_style'] : $formatter['instance_settings']['css_style'],
        '#description' => t('Provides css style for elements of row. Format: field_name_1{text-align: center;}|field_name_2{font-weight: bold;color: red;} It means field_name_1 will be aligned to center, field_name_2 will be bold and colored red'),
        '#weight' => 7,
      );
      break;
  }

  return $form;
}

/**
 * Implements hook_field_group_format_summary().
 */
function field_group_table_field_group_format_summary($group) {
  switch ($group->format_type) {
    // Provide custom information since empty values can be useful also.
    case 'table-row':
      $output = '';
      if (isset($group->format_settings['instance_settings'])) {
        $label_first_column = $group->format_settings['instance_settings']['label_first_column'] ? 'Yes' : 'No';
        $row[] = '<b>' . t('Use group label as first column') . '</b>: ' . $label_first_column;
        if (!empty($group->format_settings['instance_settings']['first_column'])) {
          $row[] = '<b>' . t('First column') . '</b>: ' . $group->format_settings['instance_settings']['first_column'];
        }
        $separate_label_vs_field = (array_key_exists('separate_label_vs_field', $group->format_settings['instance_settings']) && $group->format_settings['instance_settings']['separate_label_vs_field']) ? 'Yes' : 'No';
        $row[] = '<b>' . t('Separated label with field') . '</b>: ' . $separate_label_vs_field;
        $hide_title = (bool) $group->format_settings['instance_settings']['hide_title'] ? 'Yes' : 'No';
        $row[] = '<b>' . t('Hide title of cells') . '</b>: ' . $hide_title;
        if (!empty($group->format_settings['instance_settings']['colspan'])) {
          $row[] = '<b>' . t('Colspan') . '</b>: ' . $group->format_settings['instance_settings']['colspan'];
        }
        if (!empty($group->format_settings['instance_settings']['css_style'])) {
          $row[] = '<b>' . t('CSS Style') . '</b>: ' . $group->format_settings['instance_settings']['css_style'];
        }
        if (!empty($group->format_settings['instance_settings']['classes'])) {
          $row[] = '<b>' . t('Classes') . '</b>: ' . $group->format_settings['instance_settings']['classes'];
        }
        $output = implode('<br />', $row);
      }
      break;
  }
  if (isset($output) && !empty($output)) {
    return $output;
  }
}

/**
 * Implements hook_form_alter().
 */
function field_group_table_wrapper_form_alter(&$form, &$form_state, $form_id) {
  // Validate fieldgroup format types.
  if ($form_id == 'field_ui_field_overview_form') {
    $form['#validate'][] = 'field_group_table_wrapper_field_overview_form_validate';
  }
}

/**
 * Validate function for manage relationships between table-wrapper and table-row fieldgroups.
 *
 * @param $form
 * @param $form_state
 */
function field_group_table_wrapper_field_overview_form_validate($form, &$form_state) {
  if (array_key_exists('fields', $form_state['values'])) {
    $keys = array_keys($form_state['values']['fields']);
    $group_keys = array_filter($keys, function ($key) {
      return preg_match('/^group_.*/', $key);
    });
    foreach ($group_keys as $fieldname) {
      if ($form_state['values']['fields'][$fieldname]['format']['type'] == 'table-row') {
        $settings = $form_state['values']['fields'][$fieldname]['format_settings']['settings']['instance_settings'];
        $parent = $form_state['values']['fields'][$fieldname]['parent'];
        if ($form_state['values']['fields'][$parent]['format']['type'] != 'table-wrapper') {
          form_set_error('fields][' . $fieldname. '][format][type', $fieldname . ': Table row format can be used within Table wrapper!');
        }
        if (!empty($settings) &&  $settings['separate_label_vs_field'] == 1 && ($settings['hide_title'] == "1" || $settings['hide_title'] == TRUE)) {
          form_set_error('fields][' . $fieldname. '][format_settings][settings][instance_settings][hide_title', $fieldname . ': if "Separated label with field" settings is enabled you must not hide the title!');
        }
      }
      if ($form_state['values']['fields'][$fieldname]['format']['type'] == 'table-wrapper') {
        foreach ($group_keys as $child) {
          if ($form_state['values']['fields'][$child]['parent'] == $fieldname && $form_state['values']['fields'][$child]['format']['type'] != 'table-row') {
            form_set_error('fields][' . $child . '][format][type', $child . ': You should use Table row group format within Table wrapper!');
          }
        }
      }
    }
  }
}

/**
 * Implements hook_field_group_pre_render().
 */
function field_group_table_wrapper_field_group_pre_render(&$element, $group, &$form) {
  // We only process the 'table-wrapper', and 'table-row' group types.
  if (!in_array($group->format_type, array('table-wrapper', 'table-row'))) {
    return;
  }

  $mode = $group->mode == 'form' ? 'form' : 'display';
  $settings = $group->format_settings['instance_settings'];
  $label = check_plain($group->label);

  $field_group_header = (array_key_exists('label_as_header', $settings) && $settings['label_as_header'] == 1) ? $label : '';
  $header = array_key_exists('column_headers', $settings) ? $settings['column_headers'] : '';
  $header_separator = array_key_exists('column_separator', $settings) ? $settings['column_separator'] : '';
  $hide_title =  array_key_exists('hide_title', $settings) ? $settings['hide_title'] : '';
  $rows = $cells = array();
  if (!empty($group->children)) {
    foreach ($group->children as $delta => $child) {
      if ($group->format_type == 'table-wrapper') {
        $rows[$child] = $element[$child];
      }
      if ($group->format_type == 'table-row') {
        $cells[] = $element[$child];
      }
    }
  }
  $caption = array_key_exists('caption', $settings) ? $settings['caption'] : '';

  // Collect information for theming field group table.
  $field_group_table_properties = array(
    '#attributes' => array(
      'class' => array('field-group-format', $group->group_name),
    ),
    '#header_colspanned' => !empty($field_group_header) ? TRUE : FALSE,
  );
  if (isset($header)) {
    $field_group_table_properties['#header'] = !empty($field_group_header) ? $field_group_header : $header;
  }
  if (isset($header_separator)) {
    $field_group_table_properties['#header_separator'] = $header_separator;
  }
  if (isset($field_group_table_caption)) {
    $field_group_table_properties['#caption'] = $field_group_table_caption;
  }
  if (isset($rows)) {
    $field_group_table_properties['#rows'] = $rows;
  }
  if (isset($cells)) {
    $field_group_table_properties['#cells'] = $cells;
  }
  if (isset($hide_title)) {
    $field_group_table_properties['#hide_title'] = $hide_title;
  }
  // Create the element.
  $element += array(
    '#theme' => 'field_group_table_wrapper_render',
    '#type' => $group->format_type,
    '#title' => $group->label,
    '#mode' => $mode,
    '#groups' => array_keys($form['#groups']),
    '#settings' => $settings,
    '#attributes' => array(
      'class' => isset($settings['classes']) ?
        array_merge(
          array('field-group-table', $group->group_name),
          explode(' ', $settings['classes'])
        ) :
        array('field-group-table', $group->group_name),
    ),
    '#caption' => $caption,
    // We will add the table rows upon rendering, as doing it here means
    // messing up the field group hierarchy, which causes issues.
    '#field_group_table_wrapper' => $field_group_table_properties,
    '#entity_type' => isset($form['#entity_type']) ? $form['#entity_type'] : '',
    '#bundle' => isset($form['#bundle']) ? $form['#bundle'] : '',
  );
  // We don't render rows separately, hence we say to Drupal the rows have already printed.
  if ($group->format_type == 'table-row') {
    $element['#printed'] = TRUE;
  }
}

/**
 * Theme function to render fields by using table.
 *
 * @param $variables
 *
 * @return string
 */
function theme_field_group_table_wrapper_render($variables) {
  $element = $variables['element'];
  $output = '';
  $header = $rows = array();
  if (!empty($element['#field_group_table_wrapper'])) {
    $tablevars = array();
    $col_number = 0;
    $header_colspan = FALSE;
    if (!empty($element['#field_group_table_wrapper']['#header'])) {
      foreach (element_children($element) as $child) {
        if (strstr($element['#field_group_table_wrapper']['#header'], $child) !== FALSE && !empty($element[$child]['#title'])) {
          $element['#field_group_table_wrapper']['#header'] = str_replace($child, $element[$child]['#title'], $element['#field_group_table_wrapper']['#header']);
        }
      }
      if ($element['#field_group_table_wrapper']['#header_colspanned']) {
        $header_colspan = TRUE;
        $header = array(
          array(
            'data' => $element['#field_group_table_wrapper']['#header'],
            'colspan' => 0,
          ),
        );
      }
      else {
        $header = explode($element['#field_group_table_wrapper']['#header_separator'], $element['#field_group_table_wrapper']['#header']);
        $colspanned_header = array();
        if (!empty($header)) {
          foreach ($header as $index => $header_item) {
            preg_match('/(.+)\{colspan:(\d)\}/', $header_item, $colspan_matches);
            if ($colspan_matches) {
              $colspanned_header[$index] = array(
                'data' => $colspan_matches[1],
                'colspan' => $colspan_matches[2],
              );
            } else {
              $colspanned_header[$index] = $header_item;
            }
          }
        }
        $col_number = count($header);
        $header = $colspanned_header;
      }
    }
    if (!empty($element['#field_group_table_wrapper']['#rows'])) {
      foreach (element_children($element, TRUE) as $rowindex) {
        $cells = array();
        // Determine if we have first column.
        if (!empty($element[$rowindex]['#settings']['first_column'])) {
          $cells[] = $element[$rowindex]['#settings']['first_column'];
        }
        elseif ($element[$rowindex]['#settings']['label_first_column']) {
          $cells[] = $element[$rowindex]['#title'];
        }
        // Get the cells.
        foreach (element_children($element['#field_group_table_wrapper']['#rows'][$rowindex], TRUE) as $field) {
          $field_language = $element['#field_group_table_wrapper']['#rows'][$rowindex][$field]['#language'];
          if ($element[$rowindex]['#settings']['hide_title']) {
            if ($element['#mode'] == "form") {
              _field_group_table_wrapper_hide_element_title($element['#field_group_table_wrapper']['#rows'][$rowindex][$field], '#title_display', 'invisible');
              $element['#field_group_table_wrapper']['#rows'][$rowindex][$field]['#attributes']['class'][] = 'hidden_label';
            }
            elseif ($element['#mode'] == "display") {
              _field_group_table_wrapper_hide_element_title($element['#field_group_table_wrapper']['#rows'][$rowindex][$field], '#label_display', 'hidden');
            }
          }
          $separated_label = FALSE;
          if (array_key_exists('separate_label_vs_field', $element[$rowindex]['#settings']) && $element[$rowindex]['#settings']['separate_label_vs_field'] == 1) {
            $separated_label = TRUE;
            $cells[] = $element['#field_group_table_wrapper']['#rows'][$rowindex][$field][$field_language]['#title'];
            if ($element['#mode'] == "form") {
              _field_group_table_wrapper_hide_element_title($element['#field_group_table_wrapper']['#rows'][$rowindex][$field], '#title_display', 'invisible');
              $element['#field_group_table_wrapper']['#rows'][$rowindex][$field]['#attributes']['class'][] = 'hidden_label';
            }
            elseif ($element['#mode'] == "display") {
              _field_group_table_wrapper_hide_element_title($element['#field_group_table_wrapper']['#rows'][$rowindex][$field], '#label_display', 'hidden');
            }
          }
          $cell = array(
            'data' => drupal_render($element['#field_group_table_wrapper']['#rows'][$rowindex][$field])
          );
          if (!empty($element[$rowindex]['#settings']['colspan'])) {
            $colspans = explode('|', $element[$rowindex]['#settings']['colspan']);
            if (!empty($colspans)) {
              $value = null;
              foreach ($colspans as $colspan) {
                if (strpos($colspan, $field) !== FALSE) {
                  $value = explode(':', $colspan);
                  break;
                }
              }
              if (isset($value)) {
                // If Separated label is set we have to deduct one cell for it.
                if ($value[1] >= 2 && $separated_label) {
                  $value[1] = $value[1] - 1;
                }
                $cell['colspan'] = $value[1];
              }
            }
          }
          if (!empty($element[$rowindex]['#settings']['css_style'])) {
            $styles = explode('|', $element[$rowindex]['#settings']['css_style']);
            if (!empty($styles)) {
              $match = array();
              foreach ($styles as $style) {
                if (strpos($style, $field) !== FALSE) {
                  preg_match('/.*\{(.*)\}/', $style, $match);
                  break;
                }
              }
              if (!empty($match)) {
                $cell['style'] = $match[1];
              }
            }
          }
          $cells[] =  $cell;
        }
        // Header hasn't been set.
        if ($col_number == 0 || $col_number < count($cells)) {
          $col_number = count($cells);
        }
        $rows[] = array(
          'data' => $cells,
          'class' => explode(' ', $element[$rowindex]['#settings']['classes']),
        );
      }
      if (!empty($element['#settings']['tfoot'])) {
        $tfoot = array(
          'data' => array(
            array(
              'data' => $element['#settings']['tfoot'],
              'colspan' => $col_number,
            ),
          ),
        );
        // Footer of theme_table was introduced in 7.68
        if (VERSION <= '7.67') {
          $tfoot += array(
            'class' => array('no-striping'),
          );
          $rows[] = $tfoot;
        }
        else {
          $tablevars['footer'] = $tfoot;
        }
      }
    }
  }
  if (!empty($rows)) {
    if (!empty($header)) {
      if ($header_colspan) {
        $header[0]['colspan'] = $col_number;
      }
      $tablevars['header'] = $header;
    }
    $tablevars['rows'] = $rows;
    $tablevars['sticky'] = FALSE;
    $classes = explode(' ', $element['#settings']['classes']);
    $classes[] = 'field-group-table-wrapper';
    $classes[] = 'cols__' . $col_number;
    $tablevars['attributes'] = array('class' => $classes);
    if (!empty($element['#settings']['id'])) {
      $tablevars['attributes']['id'] = $element['#settings']['id'];
    }
    if (!empty($element['#settings']['caption'])) {
      $tablevars['caption'] = $element['#settings']['caption'];
    }
    $output .= theme('table', $tablevars);
  }
  return $output;
}

/**
 * Helper function to hide titles everywhere within an element. Recursive.
 *
 * @param array $array
 *   Array to manipulate.
 * @param string $needle_key
 *   Searched key.
 * @param string $hide_value
 *   String value to hide.
 *
 * @return array|bool
 */
function _field_group_table_wrapper_hide_element_title(&$array, $needle_key, $hide_value) {
  if (array_key_exists($needle_key, $array)) {
    $array[$needle_key] = $hide_value;
  }
  foreach ($array as $key => $value) {
    if (is_array($value)) {
      _field_group_table_wrapper_hide_element_title($array[$key], $needle_key, $hide_value);
    }
  }
}
